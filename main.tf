data "aws_caller_identity" "current" {}

locals {
  common_tags = merge(
    map("Environment", var.environment),
    map("Provisioner", "terraform")
  )
}

resource "aws_vpc" "this" {
  cidr_block = var.vpc_cidr_block

  enable_dns_support   = var.vpc_enable_dns_support
  enable_dns_hostnames = var.vpc_enable_dns_hostnames

  tags = merge(
    map("Name", var.vpc_name),
    map("Description", var.vpc_description),
    local.common_tags
  )
}

resource "aws_subnet" "public" {
  count = length(var.subnet_availability_zones)

  availability_zone       = var.subnet_availability_zones[count.index]
  cidr_block              = cidrsubnet(var.vpc_cidr_block, var.cidr_added_bits, count.index)
  map_public_ip_on_launch = "true"
  vpc_id                  = aws_vpc.this.id

  tags = merge(
    local.common_tags,
    map("Name", format("%s-public-%s", var.vpc_name, var.subnet_availability_zones[count.index])),
    map("Tier", "public"),
  )
}

resource "aws_subnet" "private" {
  count = length(var.subnet_availability_zones)

  availability_zone = var.subnet_availability_zones[count.index]
  cidr_block        = cidrsubnet(var.vpc_cidr_block, var.cidr_added_bits, count.index + var.cidr_added_bits)
  vpc_id            = aws_vpc.this.id

  tags = merge(
    local.common_tags,
    map("Name", format("%s-private-%s", var.vpc_name, var.subnet_availability_zones[count.index])),
    map("Tier", "private")
  )
}

resource "aws_internet_gateway" "this" {
  vpc_id = aws_vpc.this.id

  tags = merge(
    local.common_tags,
    map("Name", format("%s-igw", var.vpc_name))
  )
}

resource "aws_eip" "nat" {
  count = length(var.subnet_availability_zones)

  vpc = "true"

  tags = merge(
    local.common_tags,
    map("Name", format("%s-eipnat-%s", var.vpc_name, var.subnet_availability_zones[count.index]))
  )
}

resource "aws_nat_gateway" "this" {
  count = length(var.subnet_availability_zones)

  allocation_id = aws_eip.nat[count.index].id
  subnet_id     = aws_subnet.public[count.index].id

  depends_on = [
    aws_internet_gateway.this
  ]

  tags = merge(
    local.common_tags,
    map("Name", format("%s-nat-%s", var.vpc_name, var.subnet_availability_zones[count.index]))
  )
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.this.id

  tags = merge(
    local.common_tags,
    map("Name", format("%s-public-rtb", var.vpc_name)),
    map("Tier", "public"),
  )
}

resource "aws_route" "public" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.this.id
}

resource "aws_route_table_association" "public" {
  count = length(var.subnet_availability_zones)

  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table" "private" {
  count = length(var.subnet_availability_zones)

  vpc_id = aws_vpc.this.id

  tags = merge(
    local.common_tags,
    map("Name", format("%s-private-rtb-%s", var.vpc_name, var.subnet_availability_zones[count.index])),
    map("Tier", "private")
  )
}

resource "aws_route" "private" {
  count = length(var.subnet_availability_zones)

  route_table_id         = aws_route_table.private[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.this[count.index].id
}

resource "aws_route_table_association" "private" {
  count = length(var.subnet_availability_zones)

  subnet_id      = aws_subnet.private[count.index].id
  route_table_id = aws_route_table.private[count.index].id
}
